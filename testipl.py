import unittest
import Ipl
import matplotlib.pyplot as plt


class TestIpl(unittest.TestCase):
    def test_execution_matches_years(self):
        man_test1 = {'2008': 3, '2009': 1, '2010': 1, '2011': 1, '2012': 3,
                     '2013': 1, '2014': 1, '2015': 4, '2016': 1, '2017': 2}
        # man_test2={'2008': 5, '2009': 4, '2010': 5, '2011': 7, '2012': 5,
        #             '2013': 4, '2014': 5, '2015': 5, '2016': 5, '2017': 5}
        csv = '/home/ramakrishna/Desktop/IPL/IPL/test/mock_matches_Data.csv'
        func_test1 = Ipl.calculation_of_matches_years(csv)
        self.assertEqual(func_test1, man_test1)
        x = list(func_test1.keys())
        y1 = list(func_test1.values())
        y2 = list(man_test1.values())
        plt.bar(x, y1, color='green')
        plt.legend(y1, y2)
        plt.bar(x, y2, color='red')
        plt.show()

    ##########################################
    def test_execution_teams_seasons_games(self):
        man_test1 = (['Sunrisers Hyderabad', 'Mumbai Indians',
                     'Kolkata Knight Riders', 'Chennai Super Kings',
                      'Deccan Chargers', 'Royal Challengers Bangalore',
                      'Rajasthan Royals', 'Delhi Daredevils'],
                     [1, 3, 3, 4, 1, 2, 2, 1],
                     [1, 3, 3, 5, 1, 2, 2, 1])
        ''' man_test2 = (['Sunrisers Hyderabad', 'Mumbai Indians',
                     'Kolkata Knight Riders', 'Chennai Super Kings',
                      'Deccan Chargers', 'Royal Challengers Bangalore',
                      'Rajasthan Royals', 'Delhi Daredevils'],
                     [1, 3, 5, 4, 1, 2, 2, 1],
                     [1, 4, 3, 5, 1, 2, 2, 1]) '''
        csv = '/home/ramakrishna/Desktop/IPL/IPL/test/mock_matches_Data.csv'
        func_test1 = Ipl.calculation_of_games_seasons(csv)
        self.assertEqual(func_test1, man_test1)
    #############################################

    def test_execution_extra_runs(self):
        man_test1 = {'Delhi Daredevils': 3, 'Kolkata Knight Riders': 11}
        mtch_csv = 'mock_matches_Data.csv'
        del_csv = 'mock_deliveries_data.csv'
        func_test1 = Ipl.execution_extra_runs(mtch_csv, del_csv)
        self.assertEqual(func_test1, man_test1)
    ##############################################

    def test_execution_economy(self):
        man_test1 = [('HH Pandya', 2.4), ('MC Henriques', 3.0),
                     ('SL Malinga', 3.176470588235294),
                     ('DR Smith', 3.230769230769231),
                     ('CR Woakes', 3.24), ('JJ Bumrah', 3.260869565217391),
                     ('Sandeep Sharma', 3.5121951219512195),
                     ('SR Watson', 3.5454545454545454),
                     ('BCJ Cutting', 3.658536585365854)]
        mtch_csv = 'mock_matches_Data.csv'
        del_csv = 'mock_deliveries_data.csv'
        func_test1 = Ipl.execution_economy(mtch_csv, del_csv)
        self.assertEqual(func_test1, man_test1)


if __name__ == '__main__':
    unittest.main()
