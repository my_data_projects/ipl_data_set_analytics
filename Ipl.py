
import csv
import matplotlib.pyplot as plt
from collections import Counter


def read_data(file):
    # Reading Data from csv
    with open(file, 'r') as f:
        csv_reader = csv.DictReader(f)
        deliveries_list = list(csv_reader)
    return deliveries_list


def team_wise_score_calculate(file):
    # team wise scores calculation
    data = read_data(file)
    print(data[0])
    teams_data = {}
    for row in data:
        teams_data[row['batting_team']] = 0
    for row in data:
        teams_data[row['batting_team']] += int(row['total_runs'])

    return teams_data


def plot(team_data):
    # ploting the values
    plt.title("Total runs scored by team")

    x = list(team_data.keys())
    y = list(team_data.values())
    plt .plot(x, y, linewidth=2)
    plt.gcf().autofmt_xdate()
    plt.show()


def execute_Data(file):
    data = team_wise_score_calculate(file)
    plot(data)

    ############################################################


def top_batsman_Rcb(csv):
    # team wise scores calculation
    data = read_data(csv)
    batsman_data = {}
    for row in data:
        if row['batting_team'] == 'Royal Challengers Bangalore':
            batsman_data[row['batsman']] = 0
    for row in data:
        if row['batting_team'] == 'Royal Challengers Bangalore':
            batsman_data[row['batsman']] += int(row['total_runs'])
    batsman_data = dict(Counter(batsman_data).most_common(10))
    return batsman_data


def Execution_top_Rcb(csv):
    dt = top_batsman_Rcb(csv)
    plt.title('Top batsman for Royal Challengers Bangalore')
    plot(dt)

##############################################


def foriegn_umpires_count_calculation(csv):
    umpires_data = read_data(csv)
    foriegn_umpires_data = {}
    for row in umpires_data:
        print(row)
        if row['Nationality'] != '  India ':
            foriegn_umpires_data[row['Nationality']] = 0
    for row in umpires_data:
        if row['Nationality'] != '  India ':
            foriegn_umpires_data[row['Nationality']] += 1
    return foriegn_umpires_data


def Execution_Foriegn_umpires(csv):
    data = foriegn_umpires_count_calculation(csv)
    plt.title('Foreign umpire analysis')
    plot(data)

##############################################


def calculation_of_games_seasons(csv):
    data = read_data(csv)
    teams_seasons_data = {}
    for row in data:
        teams_seasons_data[row['team1']] = []

    for row in data:
        if row['season'] not in teams_seasons_data[row['team1']]:
            teams_seasons_data[row['team1']].append(row['season'])
    no_of_games = {}
    for row in data:
        no_of_games[row['team1']] = []

    for row in data:
        no_of_games[row['team1']].append(row['id'])

    teams = list(no_of_games.keys())
    seasons = list([len(x) for x in teams_seasons_data.values()])
    games = list([len(x) for x in no_of_games.values()])
    return teams, seasons, games


def plot_stack_bar(csv):
    teams, seasons, games = calculation_of_games_seasons(csv)
    plt.bar(teams, games, color='r')
    plt.bar(teams, seasons, color='b')
    plt.gcf().autofmt_xdate()
    plt.legend(["games", "seasons"])
    plt.show()


def execution_teams_seasons_games(csv):
    plot_stack_bar(csv)


################################################


def calculation_of_matches_years(csv):
    # calculating years with matches
    data = read_data(csv)

    seasons = {}
    for row in data:
        seasons[row['season']] = 0

    for row in data:
        seasons[row['season']] += 1
    return seasons


def execution_matches_years(csv):
    values = calculation_of_matches_years(csv)
    print("{:<15} {:<10} ".format('year', 'matches'))
    for year, match in values.items():
        print("{:<15} {:<10}".format(year, match))

#################################################


def calculation_of_team_wins_seasons(csv):
    # calculating years with matches
    data = read_data(csv)

    seasons = {}
    for row in data:
        seasons[row['season']] = []
    for row in data:
        seasons[row['season']].append(row['winner'])
    return seasons


def execution_team_wins_season(csv):
    result = calculation_of_team_wins_seasons(csv)
    for season, team_wins in result.items():
        print(season, ' ', list(Counter(team_wins).items()))

#################################################


def getting_ids_2016(csv):
    data = read_data(csv)
    ids_2016 = []
    for row in data:
        if row['season'] == '2016':
            ids_2016.append(row['id'])
    return ids_2016


def team_concede_extra_runs(csv, ids_2016):
    data = read_data(csv)
    teams_extra = {}
    for row in data:
        if row['match_id'] in ids_2016:
            teams_extra[row['batting_team']] = 0
    for row in data:
        if row['match_id'] in ids_2016:
            teams_extra[row['batting_team']] += int(row['extra_runs'])

    return teams_extra


def execution_extra_runs(mtch_csv, del_csv):
    ids_2016 = getting_ids_2016(mtch_csv)
    team_extra_concede = team_concede_extra_runs(del_csv, ids_2016)
    for team, extra in team_extra_concede.items():
        print(team, extra)
    return team_extra_concede
####################################################


def getting_ids_2015(matches_csv):
    data = read_data(matches_csv)
    ids_2015 = []
    for row in data:
        if row['season'] == '2015':
            ids_2015.append(row['id'])
    return ids_2015


def top_10_Economy_bowlers_2015(deliveries_csv, ids_2015):
    data = read_data(deliveries_csv)
    bowlers_overs = {}
    bowlers_runs = {}
    for row in data:
        if row['match_id'] in ids_2015:
            bowlers_overs[row['bowler']] = 0
            bowlers_runs[row['bowler']] = 0
    for row in data:
        if row['match_id'] in ids_2015:
            bowlers_overs[row['bowler']] += 1
            bowlers_runs[row['bowler']] += int(row['total_runs'])

    return bowlers_runs, bowlers_overs


def find_Economy_values(dict1, dict2):
    for key in dict2:
        if key in dict1:
            dict2[key] = dict2[key]*6 / dict1[key]
    return dict2


def execution_economy(matches_csv, deliveries_csv):
    ids_2015 = getting_ids_2015(matches_csv)
    overs, runs = top_10_Economy_bowlers_2015(deliveries_csv, ids_2015)
    val = Counter(find_Economy_values(overs, runs))
    return val.most_common()[:-10:-1]


def main():
    deliveries_csv = '/home/ramakrishna/Desktop/IPL/IPL/deliveries.csv'
    matches_csv = '/home/ramakrishna/Desktop/IPL/IPL/matches.csv'
    umpires_csv = '/home/ramakrishna/Desktop/IPL/IPL/umpires.csv'

    execute_Data(deliveries_csv)
    Execution_top_Rcb(deliveries_csv)
    Execution_Foriegn_umpires(umpires_csv)
    execution_teams_seasons_games(matches_csv)
    execution_matches_years(matches_csv)
    execution_team_wins_season(matches_csv)
    execution_extra_runs(matches_csv, deliveries_csv)
    execution_economy(matches_csv, deliveries_csv)


main()
